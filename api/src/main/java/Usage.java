import java.util.List;

public interface Usage {
    List<Human> filter(List<Human> humanList);
    boolean coincidence (List<Human> humans, Human human);
}
