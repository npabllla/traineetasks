import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class HumanUsage implements Usage {
    @Override
    public boolean coincidence(List<Human> humans, Human human) {
        return humans.contains(human);
    }

    @Override
    public List<Human> filter(List<Human> humans) {
        List<Character> firstLetters = Arrays.asList('а', 'б', 'в', 'г', 'д');
        return humans.stream()
                .filter(e -> e.getAge() <= 20 && (firstLetters.contains(
                        e.getSecondName().toLowerCase(Locale.ROOT).toCharArray()[0])))
                .collect(Collectors.toList());
    }

    public String printRepeats(List<Human> humans) {
        Map<Human, Long> mapRepeats = (humans.stream()
                .collect(Collectors.groupingBy(
                        Function.identity(), Collectors.counting()))
                )
                .entrySet()
                .stream()
                .filter(e -> e.getValue() > 1)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        Map<Human, Long> result = sortByValue(mapRepeats);
        StringBuilder sb = new StringBuilder();
        for (Human human : result.keySet()) {
            sb.append("{")
                    .append(result.get(human))
                    .append(":[{")
                    .append(human)
                    .append("}")
                    .append("}]")
                    .append(System.lineSeparator());
        }
        return sb.toString();
    }

    private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort((o1, o2) -> (o2.getValue()).compareTo(o1.getValue()));
        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
}
