import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;


public class HumanUsageTest {
    @Test
    public void whenCoincidenceReturnTrue() {
        HumanUsage humanUsage = new HumanUsage();
        List<Human> humanList = new ArrayList<>();
        Human humanPattern = new Human("A", "A", "A", 18, 'M', "2000");
        Human human1 = new Human("A", "A", "A", 18, 'M', "2000");
        Human human2 = new Human("B", "B", "B", 21, 'F', "1998");

        humanList.add(human1);
        humanList.add(human2);
        assertThat(humanUsage.coincidence(humanList, humanPattern), is(true));
    }

    @Test
    public void whenCoincidenceReturnFalse() {
        HumanUsage humanUsage = new HumanUsage();
        List<Human> humanList = new ArrayList<>();
        Human humanPattern = new Human("C", "C", "C", 18, 'M', "2000");
        Human human1 = new Human("A", "A", "A", 18, 'M', "2000");
        Human human2 = new Human("B", "B", "B", 21, 'F', "1998");
        humanList.add(human1);
        humanList.add(human2);
        assertThat(humanUsage.coincidence(humanList, humanPattern), is(false));
    }

    @Test
    public void whenFilterFindSomething() {
        HumanUsage humanUsage = new HumanUsage();
        List<Human> humanList = new ArrayList<>();
        Human human0 = new Human("C", "б", "C", 18, 'M', "2000");
        Human human1 = new Human("A", "а", "A", 18, 'M', "2000");
        Human human2 = new Human("B", "B", "B", 21, 'F', "1998");
        humanList.add(human0);
        humanList.add(human1);
        humanList.add(human2);
        List<Human> expected = new ArrayList<>();
        expected.add(human0);
        expected.add(human1);
        assertThat(humanUsage.filter(humanList), is(expected));
    }

    @Test
    public void whenFilterFindNothing() {
        HumanUsage humanUsage = new HumanUsage();
        List<Human> humanList = new ArrayList<>();
        Human human0 = new Human("C", "b", "C", 18, 'M', "2000");
        Human human1 = new Human("A", "g", "A", 18, 'M', "2000");
        Human human2 = new Human("B", "e", "B", 21, 'F', "1998");
        humanList.add(human0);
        humanList.add(human1);
        humanList.add(human2);
        List<Human> expected = new ArrayList<>();
        assertThat(humanUsage.filter(humanList), is(expected));
    }

    @Test
    public void whenPrintRepeats() {
        HumanUsage humanUsage = new HumanUsage();
        List<Human> humanList = new ArrayList<>();
        Human human0 = new Human("A", "A", "A", 18, 'M', "2000");
        Human human1 = new Human("A", "A", "A", 18, 'M', "2000");
        Human human2 = new Human("B", "B", "B", 18, 'M', "2000");
        Human human3 = new Human("B", "B", "B", 18, 'M', "2000");
        Human human4 = new Human("B", "B", "B", 18, 'M', "2000");
        Human human5 = new Human("C", "C", "C", 18, 'M', "2000");
        Human human6 = new Human("B", "B", "B", 21, 'F', "1998");
        humanList.add(human0);
        humanList.add(human1);
        humanList.add(human2);
        humanList.add(human3);
        humanList.add(human4);
        humanList.add(human5);
        humanList.add(human6);
        StringBuilder expected = new StringBuilder();
        expected.append("{")
                .append("3")
                .append(":[{")
                .append("B")
                .append("}")
                .append("}]")
                .append(System.lineSeparator())
                .append("{")
                .append("2")
                .append(":[{")
                .append("A")
                .append("}")
                .append("}]")
                .append(System.lineSeparator());
        assertThat(humanUsage.printRepeats(humanList), is(expected.toString()));
    }
}