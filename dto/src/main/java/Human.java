import java.util.Objects;

public class Human {
    private String firstName;
    private String secondName;
    private String patronymic;
    private int age;
    private char gender;
    private String birthData;

    public Human(String firstName, String secondName, String patronymic, int age, char gender, String birthData) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.patronymic = patronymic;
        this.age = age;
        this.gender = gender;
        this.birthData = birthData;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getBirthData() {
        return birthData;
    }

    public void setBirthData(String birthData) {
        this.birthData = birthData;
    }

    @Override
    public String toString() {
        return firstName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age && gender == human.gender && Objects.equals(firstName, human.firstName) && Objects.equals(secondName, human.secondName) && Objects.equals(patronymic, human.patronymic) && Objects.equals(birthData, human.birthData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, secondName, patronymic, age, gender, birthData);
    }
}
